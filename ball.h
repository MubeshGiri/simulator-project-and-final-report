#ifndef BALL_H
#define BALL_H


#include <parametrics/gmpsphere>
#include<QDebug>
class Ball : public GMlib::PSphere<float> {

public:

    using PSphere::PSphere;

    const GMlib::Vector<float,3>& get_ds()const;
    const GMlib::Vector<float,3>& get_vel()const;
 void localSimulate(double dt);
    float get_m();
    Ball(GMlib::Vector<float,3> v, float m, float r, PSurf<float,3>* s);
    void update_step(double dt);
    void update(float x);
    void set_v(const GMlib::Vector<float,3>& _setv);



private:
    GMlib::UnitVector<float,3> n;
    GMlib::Vector<float,3> ds;
    GMlib::UnitVector<float,3> rolling_b;
    GMlib::Point<float,3> p;
    GMlib::Point<float,3> q;
    GMlib::Vector<float,3> v;
    double _dt;
    double x;
    float _u;
    float _v;
    float m;
    GMlib::PSurf<float,3>* surf;


};


// Ball::Ball



Ball::Ball(GMlib::Vector<float,3> v, float m, float r, PSurf<float,3>* s):GMlib::PSphere<float>(r){
    this->v = v;
    this->surf = s;
    this->m = m;
    x=0;
}


const GMlib::Vector<float, 3> &Ball::get_ds() const
{
    return ds;
}


void Ball::set_v(const GMlib::Vector<float, 3> &_setv)
{
    v=_setv;
}


float Ball::get_m()
{
    return m;//return mass
}


const GMlib::Vector<float, 3> &Ball::get_vel() const
{
    return v;
}

void Ball::update(float x){
    this->x=x;
}
void Ball::localSimulate(double dt){


    GMlib::Vector<float,3> step = (1-x)*ds;
    this->translateParent(step);
    this->rotateParent(GMlib::Angle(step.getLength()/this->getRadius()),rolling_b); //rotation of the ball
    x=0;

}





     void Ball::update_step(double dt){
    const GMlib::Vector<float,3> g(0.0, 0.0, -9.81);

    ds = dt*v + (0.5*dt*dt)*g;
    p = this->getPos() + ds;
    surf->getClosestPoint(p,_u,_v);

    GMlib::DMatrix<GMlib::Vector<float,3> > m = surf->evaluate(_u,_v,1,1);
    n = m[0][1]^m[1][0];

    GMlib::Vector<float,3> newpos = m[0][0] + this->getRadius()*n;

    //update v and ds

    ds = newpos - this->getPos();
    rolling_b =n^ds;

    v += dt*g;
    v -= (v*n)*n;
}


#endif // TESTTORUS_H
