#ifndef BIPLANE_H
#define BIPLANE_H


 #include <parametrics/gmpplane>


class BiPlane : public GMlib::PPlane<float> {
public:
    using PPlane::PPlane;

const GMlib::Point<float,3>& getCornerPoint();


};


    // BiPlane::PPlane


    const GMlib::Point<float, 3> &BiPlane::getCornerPoint()
    {
      return this->_pt;
    }
   #endif
