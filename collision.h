#ifndef COLLISION_H
#define COLLISION_H

#include "testtorus.h"
#include "biplane.h"

class Collision
{

private:
    Ball* b[2];
    BiPlane* w;
    double x;
    bool bw;


public:

    Collision(){}
     Collision(Ball* b1, Ball* b2, double x);
     Collision(Ball* b, BiPlane* w, double x);
      bool operator < (const  Collision & other)const;
    bool operator == (const  Collision& other)const;



    Ball* getBALL(int i)const{
        return b[i];
    }

    BiPlane* getWALL()const{
        return w;
    }

    double getX()const{
        return x;
    }

    bool getIsBw()const{
        return bw;
    }

};

    // collison file


inline
 Collision:: Collision(Ball *b1, Ball *b2, double x){
    b[0]=b1;
    b[1]=b2;
    this->x=x;
    bw=false;
}

inline
 Collision:: Collision(Ball *b1, BiPlane *w, double x){
    b[0]=b1;
    this->w=w;
    this->x=x;
    bw=true;
}

inline
bool  Collision::operator < (const  Collision & other)const{

    return x < other.x;
}

inline
bool  Collision::operator == (const  Collision& other)const{
    if (b[0]== other.b[0]) return true;
    if (!other.bw && b[0]==other.b[1]) return true;
    if (!bw && b[1]== other.b[0]) return true;
    if (!bw && !other.bw && b[1] == other.b[1]) return true;
    return false;

}

#endif // TESTTORUS_H
