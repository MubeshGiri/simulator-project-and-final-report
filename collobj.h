#ifndef COLLOBJ_H
#define COLLOBJ_H

#include"testtorus.h"
class Collobj {
private:
    FirstBall *b[2];
    TestTorus *w;
    double x;
    bool bw;

public:
      Collobj(){}
      Collobj(FirstBall*b1,FirstBall*b2,double x);
      Collobj(FirstBall*b,TestTorus *w,double x);

     bool getIsBw()const;
     bool operator ==(const Collobj & other)const;
     bool operator < (const Collobj & other)const;

     FirstBall*getBall(int i )const;

     TestTorus*getWall()const;

     };


//source file .c

inline
Collobj::Collobj(FirstBall *b1, FirstBall *b2, double x){
    b[0]=b1;
    b[1]=b2;
    this->x=x;
    bw=false;
}

inline
Collobj::Collobj(FirstBall *b1, TestPlane *w, double x){
    b[0]=b1;
    this->w=w;
    this->x=x;
    bw=true;
}

inline
bool Collobj::operator < (const Collobj & other)const{

    return x < other.x;
}

inline
bool Collobj::operator == (const Collobj& other)const{
    if (b[0]== other.b[0]) return true;
    if (!other.bw && b[0]==other.b[1]) return true;
    if (!bw && b[1]== other.b[0]) return true;
    if (!bw && !other.bw && b[1] == other.b[1]) return true;
    return false;

}

FirstBall *Collobj::getBall(int i) const
{
    return b[i];
}

TestTorus *Collobj::getWall() const
{
    return w;
}

double Collobj::getX() const
{
    return x;
}

bool Collobj::getIsBw() const
{
return bw;
}


#endif // COLLOBJ_H

