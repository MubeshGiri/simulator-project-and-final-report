#ifndef CONTROLLER_H
#define CONTROLLER_H
#include "biplane.h"
#include "ball.h"
#include "collision.h"

#include <core/gmarray>
#include <parametrics/gmpbeziersurf>

class Controller:public GMlib::PSphere<float>{
    GM_SCENEOBJECT(PSphere)

 private:

        GMlib::Array < Collision> coll;
        GMlib::Array<Ball*> _ball;
        GMlib::Array<BiPlane*> _wall;
        GMlib::PBezierSurf<float>* surf;
        GMlib::Array<Ball*> _x;


public:

    Controller(GMlib::PBezierSurf<float>* surf);
     void insertBall(Ball *b);
    void insertbiPlane(BiPlane *w);


protected:
    void findBBcollision(Ball *b1, Ball *b2, GMlib::Array< Collision> &co, float x);
    void findBWcollision(Ball *b, BiPlane *w, GMlib::Array< Collision> &co, float x);
    void collisionBW(Ball *b, BiPlane *w, double dt_c);
    void collisionBB(Ball *b1, Ball *b2, double dt_c);
    void localSimulate(double dt);
    void update(float x);

};



// source file controller


void Controller::insertBall(Ball *b)
{
    this->insert(b);
    _ball += b;
}

void Controller::insertbiPlane(BiPlane *w)
{
    this->insert(w);
    _wall += w;
}


void Controller::localSimulate(double dt){

    for (int i=0; i<_ball.size();i++)
        _ball[i] ->update_step(dt);


    for (int i=0; i < _ball.size(); i++)
        for (int j=i+1; j< _ball.size(); j++)
            findBBcollision(_ball[i], _ball[j], coll, 0);

    for (int i=0; i < _ball.size(); i++)
        for (int j=0; j< _wall.size(); j++)
            findBWcollision(_ball[i], _wall[j], coll, 0);

    while(coll.getSize()>0)
    {      
        coll.sort();
        coll.makeUnique();

         Collision co = coll[0];
        coll.removeIndex(0);

        if(co.getIsBw()){

            collisionBW(co.getBALL(0), co.getWALL(), (co.getX())*dt);
            co.getBALL(0)->update(co.getX());

            for (int i = 0; i < _ball.size(); i++ )
                if(_ball[i] != co.getBALL(0))
                    findBBcollision(_ball[i], co.getBALL(0), coll, co.getX());
            for (int i = 0; i <_wall.size(); i++ )
                if(_wall[i] != co.getWALL())
                    findBWcollision(co.getBALL(0), _wall[i], coll, co.getX());
        }
        else
        {
            collisionBB(co.getBALL(0), co.getBALL(1), (co.getX())*dt);
            co.getBALL(0)->update(co.getX());
            co.getBALL(1)->update(co.getX());

            for (int i = 0; i < _ball.size(); i++ )
                if(_ball[i] != co.getBALL(0) && _ball[i] != co.getBALL(1))
                {
                    findBBcollision(_ball[i], co.getBALL(0), coll, co.getX());
                    findBBcollision(_ball[i], co.getBALL(1), coll, co.getX());
                }
            for (int i = 0; i <_wall.size(); i++ )
                if(_wall[i] != co.getWALL())
                {
                    findBWcollision(co.getBALL(0), _wall[i], coll, co.getX());
                    findBWcollision(co.getBALL(1), _wall[i], coll, co.getX());
                }
        }
    }
}

Controller::Controller(GMlib::PBezierSurf<float>* surf){

    this->toggleDefaultVisualizer();
    this->replot(5,5,0,0);
    this->setVisible(false);
    this->surf=surf;
    this->insert(surf);
}



                       // collision of balls//


void Controller::findBBcollision(Ball *b1,Ball *b2, GMlib::Array< Collision> &co, float px){

    GMlib::Vector<float,3> h = b1->get_ds() - b2->get_ds();
    GMlib::Point<float,3>   s = b1->getCenterPos()-b2->getCenterPos();

    double a = h*h;
    double b =s*h;
    double r =b1->getRadius() + b2->getRadius();
    double c =s*s - r*r;
    double k = b*b-a*c;


    if (k>0)
    {
        if (std::abs(a)<0.0000001f){
            return ;
        }
        else
        {

            double x= (-b-sqrt(k))/a;
            if (px < x && x <= 1){

            co +=  Collision(b1,b2,x);
            }

        }
    }
}


void Controller::findBWcollision(Ball *b1, BiPlane *w, GMlib::Array< Collision> &co, float px){

    GMlib::Point<float,3>  p = b1->getPos();
    GMlib::Vector<float,3> d = w->getCornerPoint() - p;

    GMlib::Vector<float,3> ds = b1->get_ds();
    GMlib::Vector<float,3>  n = w->getNormal();
    double a=ds*n;//
    double b=d*n;//
    double r=b1->getRadius();

    double x=(r+b)/a;


                   //considering the differnet case
    if(px < x && x <= 1) {
        co.insertAlways( Collision(b1,w,x));
    }
}

                         //ball and wall collision

void Controller::collisionBW(Ball *b, BiPlane *w, double dt_c){
    GMlib::Vector<float,3> new_v = b->get_vel();
    new_v -= 2*(b->get_vel()*w->getNormal())*w->getNormal();;
    b->set_v(new_v);
    b->update_step(dt_c);
}

                      //ball ball collision
void Controller::collisionBB(Ball *b1, Ball *b2, double dt_c){

    GMlib::Vector<float, 3> new_v1 = b1->get_vel();
    GMlib::Vector<float, 3> new_v2 = b2->get_vel();

    GMlib::UnitVector<float, 3> d = b2->getPos() - b1->getPos();

    float dd =d*d;

    GMlib::Vector<float, 3> v1 = ((b1->get_vel() * d)/dd)*d;
    GMlib::Vector<float, 3> v1n = b1->get_vel() - v1;

    GMlib::Vector<float, 3> v2 = ((b2->get_vel() * d)/dd)*d;
    GMlib::Vector<float, 3> v2n = (b2->get_vel()) - v2;

    float m1 = b1->get_m();
    float m2 = b2->get_m();

    GMlib::Vector<float, 3> _v1 = ((m1 - m2)/(m1 + m2))*v1 + ((2*m2)/(m1 + m2))*v2;//ball and ball impact calc.
    GMlib::Vector<float, 3> _v2 = ((m2 - m1)/(m1 + m2))*v2 + ((2*m1)/(m1 + m2))*v1;//calculated by using the combination of mass and velocity

    new_v1 = _v1 + v1n;
    new_v2 = _v2 + v2n;



    b1->set_v(new_v1);//set velocity for ball1


    b1->update_step(dt_c);//update ds


    b2->set_v(new_v2);//set velocity for ball2

    b2->update_step(dt_c);//update ds

}




#endif // CONTROLLER_H
