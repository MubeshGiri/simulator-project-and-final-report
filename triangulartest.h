#ifndef TRIANGULARTEST
#define TRIANGULARTEST

/*! \file gmptrianguloidtrefoil.h
 *
 *  Interface for the PTrianguloidTrefoil class.
 */

#ifndef __gmPTRIANGULOIDTREFOIL_H__
#define __gmPTRIANGULOIDTREFOIL_H__

#include "../gmlib/modules/parametrics/src/gmpsurf.h"




  template <typename T>
  class PTrianguloidTrefoil : public GMlib::PSurf<T,3> {
    GM_SCENEOBJECT(PTrianguloidTrefoil)
  public:
    PTrianguloidTrefoil(const GMlib::Point<T,3>& P1,const GMlib::Point<T,3>& P2 ,const GMlib::Point<T,3>& P3,const GMlib::Point<T,3>& P4);
    PTrianguloidTrefoil( const PTrianguloidTrefoil<T>& copy );
    virtual ~PTrianguloidTrefoil();


  protected:
    GMlib::Point <T,3>     _p1;
    GMlib::Point <T,3>     _p2;
    GMlib::Point <T,3>     _p3;
    GMlib::Point <T,3>     _p4;
    void              eval(T u, T v, int d1, int d2, bool lu = true, bool lv = true );
    T                 getEndPU();
    T                 getEndPV();
    T                 getStartPU();
    T                 getStartPV();

  }; // END class PTrianguloidTrefoil




// Include PTrianguloidTrefoil class function implementations
#include "triangulartest.c"



#endif // __gmPTRIANGULOIDTREFOIL_H__


#endif // TRIANGULARTEST

